/*
 Navicat Premium Data Transfer

 Source Server         : Demo
 Source Server Type    : MongoDB
 Source Server Version : 60001 (6.0.1)
 Source Host           : localhost:27017
 Source Schema         : login

 Target Server Type    : MongoDB
 Target Server Version : 60001 (6.0.1)
 File Encoding         : 65001

 Date: 12/10/2022 11:05:31
*/


// ----------------------------
// Collection structure for user
// ----------------------------
db.getCollection("user").drop();
db.createCollection("user");

// ----------------------------
// Documents of user
// ----------------------------
db.getCollection("user").insert([ {
    _id: ObjectId("634617c49d110f9df6014f83"),
    username: "bcn",
    password: "123"
} ]);
db.getCollection("user").insert([ {
    _id: ObjectId("634618219d110f9df6014f85"),
    username: "alg",
    password: 123
} ]);
db.getCollection("user").insert([ {
    _id: ObjectId("63462e5c39561e5497cfd4bf"),
    username: "aaa",
    password: "123",
    _class: "com.bcn.springboot_mongodb_login.pojo.User"
} ]);
