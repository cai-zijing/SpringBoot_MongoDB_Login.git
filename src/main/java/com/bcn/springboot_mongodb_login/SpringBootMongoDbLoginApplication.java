package com.bcn.springboot_mongodb_login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMongoDbLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMongoDbLoginApplication.class, args);
    }

}
