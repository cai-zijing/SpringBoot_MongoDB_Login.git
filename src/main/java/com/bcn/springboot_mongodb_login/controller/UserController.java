package com.bcn.springboot_mongodb_login.controller;

import com.bcn.springboot_mongodb_login.pojo.User;
import com.bcn.springboot_mongodb_login.service.Impl.UserServiceImpl;
import com.bcn.springboot_mongodb_login.util.Result;
import com.bcn.springboot_mongodb_login.util.ResultUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 大白菜
 * @date Created in 2022/10/12 10:17
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserServiceImpl userServiceImpl;

    @RequestMapping("/login")
    public Result login(@RequestParam String username, @RequestParam String password) {
        String msg = userServiceImpl.loginService(username,password);
        if(("SUCCESS").equals(msg)){
            return ResultUtil.success("登录成功");
        }else{
            return ResultUtil.error(msg);
        }
    }

    @RequestMapping("/register")
    public Result login(@RequestBody User user) {
        String msg = userServiceImpl.registerService(user);
        if(("SUCCESS").equals(msg)){
            return ResultUtil.success("注册成功");
        }else{
            return ResultUtil.error(msg);
        }
    }

}
