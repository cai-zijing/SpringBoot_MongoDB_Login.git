package com.bcn.springboot_mongodb_login.mapper;

import com.bcn.springboot_mongodb_login.pojo.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 大白菜
 * @date Created in 2022/10/12 9:34
 */
@Repository
public interface UserMapper extends MongoRepository<User,String> {

    User findByUsername(String username);

}
