package com.bcn.springboot_mongodb_login.service.Impl;

import com.bcn.springboot_mongodb_login.mapper.UserMapper;
import com.bcn.springboot_mongodb_login.pojo.User;
import com.bcn.springboot_mongodb_login.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 大白菜
 * @date Created in 2022/10/12 10:19
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public String loginService(String username, String password) {
        User user = userMapper.findByUsername(username);
        System.out.println(user);
        System.out.println(username);
        System.out.println(password);

        if (user == null) {
            return "用户不存在";
        } else {
            if (password.equals(user.getPassword())) {
                return "SUCCESS";
            } else {
                return "用户密码错误";
            }
        }
    }


    @Override
    public String registerService(User user) {
        String tempUnm = user.getUsername();
        User tempUser = userMapper.findByUsername(tempUnm);
        if (tempUser != null) {
            return "用户已存在";
        } else {
            userMapper.save(user);
            return "SUCCESS";
        }
    }
}
