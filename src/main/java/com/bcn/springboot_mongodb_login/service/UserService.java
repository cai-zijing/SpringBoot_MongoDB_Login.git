package com.bcn.springboot_mongodb_login.service;

import com.bcn.springboot_mongodb_login.pojo.User;

/**
 * @author 大白菜
 * @date Created in 2022/10/12 10:18
 */
public interface UserService {
    String loginService(String username,String password);
    String registerService(User user);
}
